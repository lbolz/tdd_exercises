class BowlingGame:
   
    def __init__(self):
        self.frames = []

    def roll(self, pins):
        if not self.frames or len(self.frames[-1]) == 2:
            self.frames.append([pins])
        else:
            self.frames[-1].append(pins)

    def score(self):
        total_score = 0
        for num, frame in enumerate(self.frames):
            if sum(frame) == 10:
                total_score += 10 + self.frames[num+1][0]
            else:
                total_score += sum(frame)
        return total_score

