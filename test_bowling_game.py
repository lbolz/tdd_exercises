from unittest import TestCase

from bowling_game import BowlingGame

class TestBowlingGame(TestCase):
    
    def setUp(self):
        self.game = BowlingGame()

    def roll_many(self, num_rolls, pins):
        for roll in range(num_rolls):
            self.game.roll(pins)

    def test_gutter_game(self):
        self.roll_many(20, 0)
        self.assertEqual(self.game.score(), 0)

    def test_all_ones_game(self):
        self.roll_many(20, 1)
        self.assertEqual(self.game.score(), 20)

    def test_mixed_rolls(self):
        self.roll_many(10, 2)
        self.roll_many(10, 4)
        self.assertEqual(self.game.score(), 60)

    def test_one_spare(self):
        self.game.roll(7)
        self.game.roll(3) # a spare
        self.roll_many(18, 1)
        self.assertEqual(self.game.score(), 29)

